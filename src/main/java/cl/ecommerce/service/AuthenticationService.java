package cl.ecommerce.service;

import cl.ecommerce.constants.Constants;
import cl.ecommerce.dto.AuthenticationRequestDTO;
import cl.ecommerce.dto.AuthenticationResponseDTO;
import cl.ecommerce.enums.ErrorEnum;
import cl.ecommerce.exception.ErrorNegocioException;
import cl.ecommerce.model.Autorizacion;
import cl.ecommerce.model.Usuario;
import cl.ecommerce.repository.AutorizacionRepository;
import cl.ecommerce.repository.UsuarioRepository;
import cl.ecommerce.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final AutorizacionRepository autorizacionRepository;
    private final UsuarioRepository usuarioRepository;
    private final JwtTokenUtil jwtTokenUtil;

    public AuthenticationResponseDTO auth(final String authorization, final AuthenticationRequestDTO authenticationRequestDTO) {

        if(ObjectUtils.isEmpty(authorization) || !authorization.toUpperCase().startsWith(Constants.BASIC.toUpperCase() + Constants.SPACE)) {
            throw new ErrorNegocioException(ErrorEnum.EXT001.getCodigo(), ErrorEnum.EXT001.getMensaje());
        }

        final String basic = authorization.split(" ")[1];
        final boolean authorizationExists = autorizacionRepository.existsAutorizacionByBasic(basic);

        if(!authorizationExists) {
            throw new ErrorNegocioException(ErrorEnum.EXT001.getCodigo(), ErrorEnum.EXT001.getMensaje());
        }

        final boolean userExists = usuarioRepository.existsUsuarioByRutAndPassword(authenticationRequestDTO.getUsername(), authenticationRequestDTO.getPassword());

        if(!userExists) {
            throw new ErrorNegocioException(ErrorEnum.EXT001.getCodigo(), ErrorEnum.EXT001.getMensaje());
        }

        final Autorizacion autorizacion = autorizacionRepository.findAutorizacionByBasic(basic);
        final List<String> roles = Collections.singletonList(autorizacion.getRol());
        final Usuario usuario = usuarioRepository.findUsuarioByRutAndPassword(authenticationRequestDTO.getUsername(), authenticationRequestDTO.getPassword()).get();

        final String token = jwtTokenUtil.generateToken(usuario.getRut(), usuario.getUsername(), roles);
        final int expiration = jwtTokenUtil.getExpirationFromToken(token);

        return AuthenticationResponseDTO
                .builder()
                .accessToken(token)
                .expiration(expiration)
                .roles(roles)
                .build();
    }
}