package cl.ecommerce.service;

import cl.ecommerce.dto.CategoriaRequestDTO;
import cl.ecommerce.dto.CategoriaResponseDTO;
import cl.ecommerce.dto.PrimeraSubCategoriaDTO;
import cl.ecommerce.dto.SegundaSubCategoriaDTO;
import cl.ecommerce.model.Categoria;
import cl.ecommerce.model.PrimeraSubCategoria;
import cl.ecommerce.model.SegundaSubCategoria;
import cl.ecommerce.repository.CategoriaRepository;
import cl.ecommerce.repository.PrimeraSubCategoriaRepository;
import cl.ecommerce.repository.SegundaSubCategoriaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoriaService {
    private final CategoriaRepository categoriaRepository;
    private final PrimeraSubCategoriaRepository primeraSubCategoriaRepository;
    private final SegundaSubCategoriaRepository segundaSubCategoriaRepository;

    public List<CategoriaResponseDTO> findAll() {
        List<Categoria> categorias = categoriaRepository.findAll();

        List<CategoriaResponseDTO> categoriasDTOS = categorias
                .stream()
                .map(categoria -> CategoriaResponseDTO
                        .builder()
                        .nombre(categoria.getNombre())
                        .descripcion(categoria.getDescripcion())
                        .subCategorias(categoria.getSubCategorias()
                                .stream()
                                .map(primeraSubCategoria -> PrimeraSubCategoriaDTO
                                        .builder()
                                        .nombre(primeraSubCategoria.getNombre())
                                        .descripcion(primeraSubCategoria.getDescripcion())
                                        .subCategorias(primeraSubCategoria.getSubCategorias()
                                                .stream()
                                                .map(segundaSubCategoria -> SegundaSubCategoriaDTO
                                                        .builder()
                                                        .nombre(segundaSubCategoria.getNombre())
                                                        .descripcion(segundaSubCategoria.getDescripcion())
                                                        .build())
                                                .collect(Collectors.toList()))
                                        .build())
                                .collect(Collectors.toList()))
                        .build())
                .collect(Collectors.toList());

        return categoriasDTOS;
    }

    public Optional<CategoriaResponseDTO> findByNombre(String nombre) {
        Optional<Categoria> optionalCategoria = categoriaRepository.findByNombreContainingIgnoreCase(nombre);

        if(!optionalCategoria.isPresent()) {
            return Optional.empty();
        }

        CategoriaResponseDTO categoriaRequestDTO = CategoriaResponseDTO
                .builder()
                .nombre(optionalCategoria.get().getNombre())
                .descripcion(optionalCategoria.get().getDescripcion())
                .subCategorias(optionalCategoria.get().getSubCategorias()
                        .stream()
                        .map(primeraSubCategoria -> PrimeraSubCategoriaDTO
                                .builder()
                                .nombre(primeraSubCategoria.getNombre())
                                .descripcion(primeraSubCategoria.getDescripcion())
                                .subCategorias(primeraSubCategoria.getSubCategorias()
                                        .stream()
                                        .map(segundaSubCategoria -> SegundaSubCategoriaDTO
                                                .builder()
                                                .nombre(segundaSubCategoria.getNombre())
                                                .descripcion(segundaSubCategoria.getDescripcion())
                                                .build())
                                        .collect(Collectors.toList()))
                                .build())
                        .collect(Collectors.toList()))
                .build();

        return Optional.of(categoriaRequestDTO);
    }

    public List<SegundaSubCategoriaDTO> findAllSegundaSubCategoriaByNombreCategoriaAndNombrePrimeraSubCategoria(String nombre, String nombrePrimeraSubCategoria) {
        Optional<Categoria> optionalCategoria = categoriaRepository.findByNombreContainingIgnoreCase(nombre);

        if(!optionalCategoria.isPresent()) {
            return new ArrayList<>();
        }

        Optional<PrimeraSubCategoria> optionalPrimeraSubCategoria = primeraSubCategoriaRepository.findByNombreContainingIgnoreCase(nombrePrimeraSubCategoria);

        if(!optionalPrimeraSubCategoria.isPresent()) {
            return new ArrayList<>();
        }

        List<SegundaSubCategoriaDTO> segundaSubCategoriaDTOS = optionalPrimeraSubCategoria.get().getSubCategorias()
                .stream()
                .map(segundaSubCategoria -> SegundaSubCategoriaDTO
                        .builder()
                        .nombre(segundaSubCategoria.getNombre())
                        .descripcion(segundaSubCategoria.getDescripcion())
                        .build())
                .collect(Collectors.toList());

        return segundaSubCategoriaDTOS;
    }

    public boolean save(CategoriaRequestDTO categoriaRequestDTO) {
        Categoria categoriaACrear = Categoria
                .builder()
                .nombre(categoriaRequestDTO.getNombre())
                .descripcion(categoriaRequestDTO.getDescripcion())
                .build();

        categoriaRepository.save(categoriaACrear);

        boolean categoriaCreadaExitosamente = categoriaRepository.existsCategoriaByNombreContainingIgnoreCase(categoriaACrear.getNombre());

        if(categoriaCreadaExitosamente) {
            categoriaRequestDTO.getSubCategorias().forEach(primeraSubCategoriaDTO -> {
                PrimeraSubCategoria primeraSubCategoriaACrear = PrimeraSubCategoria
                        .builder()
                        .idCategoria(categoriaACrear.getId())
                        .nombre(primeraSubCategoriaDTO.getNombre())
                        .descripcion(primeraSubCategoriaDTO.getDescripcion())
                        .build();
                primeraSubCategoriaRepository.save(primeraSubCategoriaACrear);

                boolean primeraSubCategoriaCreadaExitosamente = primeraSubCategoriaRepository.existsPrimeraSubCategoriaByNombreContainingIgnoreCase(primeraSubCategoriaACrear.getNombre());

                if(primeraSubCategoriaCreadaExitosamente) {
                    List<SegundaSubCategoria> segundaSubCategorias = primeraSubCategoriaDTO
                            .getSubCategorias()
                            .stream()
                            .map(segundaSubCategoriaDTO -> SegundaSubCategoria
                                    .builder()
                                    .idPrimeraSubCategoria(primeraSubCategoriaACrear.getId())
                                    .nombre(segundaSubCategoriaDTO.getNombre())
                                    .descripcion(segundaSubCategoriaDTO.getDescripcion())
                                    .build())
                            .collect(Collectors.toList());
                    segundaSubCategoriaRepository.saveAll(segundaSubCategorias);
                }
            });
        }

        return categoriaCreadaExitosamente;
    }
}