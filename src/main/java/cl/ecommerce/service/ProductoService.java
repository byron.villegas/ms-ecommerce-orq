package cl.ecommerce.service;

import cl.ecommerce.dto.ImagenDTO;
import cl.ecommerce.dto.ProductoRequestDTO;
import cl.ecommerce.dto.ProductoResponseDTO;
import cl.ecommerce.model.Imagen;
import cl.ecommerce.model.Producto;
import cl.ecommerce.repository.ImagenRepository;
import cl.ecommerce.repository.ProductoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductoService {
    private final ProductoRepository productoRepository;
    private final ImagenRepository imagenRepository;

    public List<ProductoResponseDTO> findAll() {
        List<Producto> productos = productoRepository.findAll();

        log.info("### Productos Encontrados () --> [{}]", productos);

        List<ProductoResponseDTO> productoRequestDTOS = productos
                .stream()
                .map(producto -> ProductoResponseDTO
                        .builder()
                        .sku(producto.getSku())
                        .nombre(producto.getNombre())
                        .descripcion(producto.getDescripcion())
                        .imagenes(producto
                                .getImagenes()
                                .stream()
                                .map(imagen -> ImagenDTO
                                        .builder()
                                        .url(imagen.getUrl())
                                        .descripcion(imagen.getDescripcion())
                                        .build())
                                .collect(Collectors.toList()))
                        .build())
                .collect(Collectors.toList());

        log.info("### Productos Transformados () --> [{}]", productos);

        return productoRequestDTOS;
    }

    public Optional<ProductoResponseDTO> findBySku(Long sku) {
        Optional<Producto> optionalProducto = productoRepository.findBySku(sku);

        log.info("### Encontrar Producto por Sku () --> [{}]", sku);

        if(!optionalProducto.isPresent()) {
            return Optional.empty();
        }

        ProductoResponseDTO productoResponseDTO = ProductoResponseDTO
                .builder()
                .sku(optionalProducto.get().getSku())
                .nombre(optionalProducto.get().getNombre())
                .descripcion(optionalProducto.get().getDescripcion())
                .precio(optionalProducto.get().getPrecio())
                .imagenes(optionalProducto
                        .get()
                        .getImagenes()
                        .stream()
                        .map(imagen -> ImagenDTO
                                .builder()
                                .url(imagen.getUrl())
                                .descripcion(imagen.getDescripcion())
                                .build())
                        .collect(Collectors.toList()))
                .build();

        log.info("### Producto Encontrado () --> [{}]", productoResponseDTO);

        return Optional.of(productoResponseDTO);
    }

    public boolean save(ProductoRequestDTO productoRequestDTO) {

        log.info("### Guardar Producto () --> [{}]", productoRequestDTO);

        Producto productoACrear = Producto
                .builder()
                .sku(productoRequestDTO.getSku())
                .nombre(productoRequestDTO.getNombre())
                .descripcion(productoRequestDTO.getDescripcion())
                .precio(productoRequestDTO.getPrecio())
                .build();

        log.info("### Producto a Crear () --> [{}]", productoACrear);

        Producto productoCreado = productoRepository.save(productoACrear);

        boolean creadoExitosamente = productoCreado.getId() > 0;

        log.info("### Resultado () --> [{}]", creadoExitosamente);

        if(creadoExitosamente) {
            List<Imagen> imagenes = productoRequestDTO
                    .getImagenes()
                    .stream()
                    .map(imagenDTO -> Imagen
                            .builder()
                            .idProducto(productoCreado.getId())
                            .url(imagenDTO.getUrl())
                            .descripcion(imagenDTO.getDescripcion())
                            .build())
                    .collect(Collectors.toList());

            log.info("### Imagenes a crear () --> [{}]", imagenes);

            imagenRepository.saveAll(imagenes);
        }

        return creadoExitosamente;
    }
}