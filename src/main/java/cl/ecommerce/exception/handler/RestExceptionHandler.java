package cl.ecommerce.exception.handler;

import cl.ecommerce.dto.ErrorDTO;
import cl.ecommerce.dto.ValidacionesParametrosErrorDTO;
import cl.ecommerce.exception.ErrorNegocioException;
import cl.ecommerce.exception.ErrorTecnicoException;
import cl.ecommerce.util.ValidacionesParametrosUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class RestExceptionHandler {
    private final String ERROR = "Error";
    private final String ERROR_DEL_SERVIDOR = "Error del servidor";

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException constraintViolationException) {
        constraintViolationException.getConstraintViolations();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException) {
        final ValidacionesParametrosErrorDTO validacionesParametrosErrorDTO = ValidacionesParametrosErrorDTO
                .builder()
                .mensaje("Error en los parametros de entrada")
                .detalle(ValidacionesParametrosUtil.generarDetallesParametrosErrores(methodArgumentNotValidException.getFieldErrors()))
                .build();

        return new ResponseEntity<>(validacionesParametrosErrorDTO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(Exception exception) {
        final ErrorDTO errorDTO = ErrorDTO
                .builder()
                .codigo(ERROR)
                .mensaje(ERROR_DEL_SERVIDOR)
                .build();
        return new ResponseEntity<>(errorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ErrorNegocioException.class)
    protected ResponseEntity<Object> handleErrorNegocioException(ErrorNegocioException errorNegocioException) {
        final ErrorDTO errorDTO = ErrorDTO
                .builder()
                .codigo(errorNegocioException.getCodigo())
                .mensaje(errorNegocioException.getMensaje())
                .build();
        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ErrorTecnicoException.class)
    protected ResponseEntity<Object> handleErrorTecnicoException(ErrorTecnicoException errorTecnicoException) {
        final ErrorDTO errorDTO = ErrorDTO
                .builder()
                .codigo(errorTecnicoException.getCodigo())
                .mensaje(ERROR_DEL_SERVIDOR)
                .build();
        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
    }
}