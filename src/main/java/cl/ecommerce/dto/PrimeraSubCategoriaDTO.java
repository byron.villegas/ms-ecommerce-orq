package cl.ecommerce.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PrimeraSubCategoriaDTO {
    private String nombre;
    private String descripcion;
    List<SegundaSubCategoriaDTO> subCategorias;
}