package cl.ecommerce.application;

import cl.ecommerce.model.Autorizacion;
import cl.ecommerce.model.Usuario;
import cl.ecommerce.repository.AutorizacionRepository;
import cl.ecommerce.repository.UsuarioRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {"cl.ecommerce"})
@EntityScan(basePackages = {"cl.ecommerce"})
@EnableJpaRepositories(basePackages = {"cl.ecommerce"})
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class MsEcommerceOrqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsEcommerceOrqApplication.class, args);
    }

    @Bean // CREA ROLES Y USUARIO POR DEFECTO
    public CommandLineRunner innit(AutorizacionRepository autorizacionRepository, UsuarioRepository usuarioRepository) {
        return (args) -> {
            if(!autorizacionRepository.findById(1L).isPresent()) {
                Autorizacion autorizacion = Autorizacion
                        .builder()
                        .basic("EST9jmXp4CPOvSbpwWENDgIS4Nk1BeNcYfPvkON+OCU=")
                        .rol("ROLE_USER")
                        .build();
                autorizacionRepository.save(autorizacion);
            }
            if(!autorizacionRepository.findById(2L).isPresent()) {
                Autorizacion autorizacion = Autorizacion
                        .builder()
                        .basic("PFHjqu0mhAoCc8Ug4LFa1g==")
                        .rol("ROLE_ADMINISTRATOR")
                        .build();
                autorizacionRepository.save(autorizacion);
            }

            if (!usuarioRepository.findById(1L).isPresent()) {
                Usuario usuario = Usuario
                        .builder()
                        .rut("11.111.111-1")
                        .username("jbodoq")
                        .nombres("Juan Carlos")
                        .apellidoPaterno("Bodoque")
                        .apellidoMaterno("Bodoque")
                        .email("jbodoq@gmail.com")
                        .telefono("987654321")
                        .estado("VIG")
                        .password("admin123")
                        .build();
                usuarioRepository.save(usuario);
            }
        };
    }
}