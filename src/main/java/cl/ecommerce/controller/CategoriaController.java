package cl.ecommerce.controller;

import cl.ecommerce.dto.CategoriaRequestDTO;
import cl.ecommerce.dto.CategoriaResponseDTO;
import cl.ecommerce.dto.SegundaSubCategoriaDTO;
import cl.ecommerce.service.CategoriaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("categorias")
@RequiredArgsConstructor
public class CategoriaController {
    private final CategoriaService categoriaService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoriaResponseDTO> findAll() {
        return categoriaService.findAll();
    }

    @GetMapping(value = "{nombreCategoria}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoriaResponseDTO> findByNombre(@PathVariable String nombreCategoria) {
        Optional<CategoriaResponseDTO> optionalProducto = categoriaService.findByNombre(nombreCategoria);
        return optionalProducto.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @GetMapping(value = "{nombreCategoria}/{nombrePrimeraSubCategoria}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SegundaSubCategoriaDTO> findAllSegundaSubCategoriaByNombreCategoriaAndNombrePrimeraSubCategoria(@PathVariable String nombreCategoria, @PathVariable String nombrePrimeraSubCategoria) {
        return categoriaService.findAllSegundaSubCategoriaByNombreCategoriaAndNombrePrimeraSubCategoria(nombreCategoria, nombrePrimeraSubCategoria);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> save(@RequestBody CategoriaRequestDTO categoriaRequestDTO) {
        boolean fueCreado = categoriaService.save(categoriaRequestDTO);

        HttpStatus resultado = fueCreado ? HttpStatus.CREATED : HttpStatus.INTERNAL_SERVER_ERROR;

        return ResponseEntity.status(resultado).build();
    }
}