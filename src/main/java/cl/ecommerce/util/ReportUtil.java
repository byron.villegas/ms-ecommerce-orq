package cl.ecommerce.util;

import cl.ecommerce.constants.Constants;
import cl.ecommerce.exception.ErrorTecnicoException;
import cl.ecommerce.report.ReportExporterFactory;
import cl.ecommerce.report.ReportGenericExporter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.stereotype.Component;
import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class ReportUtil {
    private final DataSource datasource;

    public byte[] generateReportByNameAndTypeAndParameters(String name, String type, Map<String, Object> parameters) {
        try {
            Connection connection = datasource.getConnection();

            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            InputStream inputStream = getClass().getResourceAsStream(Constants.JASPER_REPORTS_FOLDER + name + Constants.DOT + Constants.JRXML.toLowerCase());

            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);

            ReportGenericExporter reportGenericExporter = ReportExporterFactory.getReportExporter(type);

            reportGenericExporter.generateExporter(jasperPrint, byteArrayOutputStream);

            connection.close();

            return byteArrayOutputStream.toByteArray();
        } catch (SQLException | JRException ex) {
            log.error(ex.getMessage());
            throw new ErrorTecnicoException("EAGR", "Error al generar reporte");
        }
    }
}