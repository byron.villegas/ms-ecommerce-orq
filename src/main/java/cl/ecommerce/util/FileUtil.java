package cl.ecommerce.util;

import org.springframework.core.io.ClassPathResource;

public class FileUtil {

    private FileUtil() {

    }

    public static boolean isFileExistsInResources(String filename, String path) {
        return new ClassPathResource(path + filename).exists();
    }
}