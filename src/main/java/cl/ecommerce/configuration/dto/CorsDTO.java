package cl.ecommerce.configuration.dto;

import lombok.Data;

@Data
public class CorsDTO {
    private boolean allowCredentials;
    private String allowedOrigin;
    private String allowedHeader;
    private String AllowedMethod;
    private String basePath;
}