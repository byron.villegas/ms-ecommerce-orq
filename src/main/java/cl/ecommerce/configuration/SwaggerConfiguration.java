package cl.ecommerce.configuration;

import cl.ecommerce.configuration.dto.ContactDTO;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("swagger")
public class SwaggerConfiguration {
    private String basePackage;
    private String title;
    private String description;
    private ContactDTO contact;
    private String license;
    private String licenseUrl;
    private String version;
}