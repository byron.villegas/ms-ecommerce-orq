package cl.ecommerce.configuration;

import cl.ecommerce.configuration.dto.CorsDTO;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties("security")
public class SecurityConfiguration {
    private List<String> whiteListUrls;
    private CorsDTO cors;
}