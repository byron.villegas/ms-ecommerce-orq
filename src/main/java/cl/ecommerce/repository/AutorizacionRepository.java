package cl.ecommerce.repository;

import cl.ecommerce.model.Autorizacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutorizacionRepository extends JpaRepository<Autorizacion, Long> {
    boolean existsAutorizacionByBasic(String basic);
    Autorizacion findAutorizacionByBasic(String basic);
}
