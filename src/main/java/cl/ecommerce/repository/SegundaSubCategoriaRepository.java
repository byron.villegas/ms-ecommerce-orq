package cl.ecommerce.repository;

import cl.ecommerce.model.SegundaSubCategoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SegundaSubCategoriaRepository extends JpaRepository<SegundaSubCategoria, Long> {

}