package cl.ecommerce.repository;

import cl.ecommerce.model.PrimeraSubCategoria;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface PrimeraSubCategoriaRepository extends JpaRepository<PrimeraSubCategoria, Long> {
    Optional<PrimeraSubCategoria> findByNombreContainingIgnoreCase(String nombre);
    boolean existsPrimeraSubCategoriaByNombreContainingIgnoreCase(String nombre);
}