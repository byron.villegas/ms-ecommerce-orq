package cl.ecommerce.repository;

import cl.ecommerce.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {
    Optional<Categoria> findByNombreContainingIgnoreCase(String nombre);
    boolean existsCategoriaByNombreContainingIgnoreCase(String nombre);
}