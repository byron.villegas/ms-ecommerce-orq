package cl.ecommerce.service

import cl.ecommerce.dto.CategoriaRequestDTO
import cl.ecommerce.dto.PrimeraSubCategoriaDTO
import cl.ecommerce.dto.SegundaSubCategoriaDTO
import cl.ecommerce.model.Categoria
import cl.ecommerce.model.PrimeraSubCategoria
import cl.ecommerce.model.SegundaSubCategoria
import cl.ecommerce.repository.CategoriaRepository
import cl.ecommerce.repository.PrimeraSubCategoriaRepository
import cl.ecommerce.repository.SegundaSubCategoriaRepository
import spock.lang.Specification

class CategoriaServiceSpec extends Specification {
    def categoriaRepository = Stub(CategoriaRepository)
    def primeraSubCategoriaRepository = Stub(PrimeraSubCategoriaRepository)
    def segundaSubCategoriaRepository = Stub(SegundaSubCategoriaRepository)
    CategoriaService categoriaService = new CategoriaService(categoriaRepository, primeraSubCategoriaRepository, segundaSubCategoriaRepository)

    def setup() {

    }

    def "findAll"() {
        given: "Una solicitud de consulta general"
        def categoria = Categoria
                .builder()
                .id(1)
                .nombre("A")
                .descripcion("A")
                .subCategorias(Arrays.asList(PrimeraSubCategoria
                        .builder()
                        .id(1)
                        .idCategoria(1)
                        .nombre("B")
                        .descripcion("B")
                        .subCategorias(Arrays.asList(SegundaSubCategoria
                                .builder()
                                .id(1)
                                .idPrimeraSubCategoria(1)
                                .nombre("C")
                                .descripcion("C")
                                .build()))
                        .build()))
                .build()

        when: "Consultamos"
        categoriaRepository.findAll() >> Arrays.asList(categoria)
        def response = categoriaService.findAll()

        then: "Validamos que sea de manera correcta"
        response != null
        !response.isEmpty()
    }

    def "findByNombre is present"() {
        given: "Una solicitud para encontrar por nombre"
        def nombre = "A"
        def categoria = Categoria
                .builder()
                .id(1)
                .nombre("A")
                .descripcion("A")
                .subCategorias(Arrays.asList(PrimeraSubCategoria
                        .builder()
                        .id(1)
                        .idCategoria(1)
                        .nombre("B")
                        .descripcion("B")
                        .subCategorias(Arrays.asList(SegundaSubCategoria
                                .builder()
                                .id(1)
                                .idPrimeraSubCategoria(1)
                                .nombre("C")
                                .descripcion("C")
                                .build()))
                        .build()))
                .build()

        when: "Consultamos"
        categoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.of(categoria)
        def response = categoriaService.findByNombre(nombre)

        then: "Validamos que sea de manera correcta"
        response.isPresent()
    }

    def "findByNombre is not present"() {
        given: "Una solicitud para encontrar por nombre"
        def nombre = "A"

        when: "Consultamos"
        categoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.empty()
        def response = categoriaService.findByNombre(nombre)

        then: "Validamos que sea de manera correcta"
        !response.isPresent()
    }

    def "findAll subcategoria by nombre categoria y primera subcategoria"() {
        given: "Una solicitud para buscar todas las subcategorias por nombre categoria y primera subcategoria"
        def nombreCategoria = "A"
        def nombrePrimeraSubCategoria = "B"
        def categoria = Categoria
                .builder()
                .build()
        def primeraSubCategoria = PrimeraSubCategoria
                .builder()
                .subCategorias(Collections.singletonList(SegundaSubCategoria
                        .builder()
                        .nombre("A")
                        .descripcion("B")
                        .build()))
                .build()

        when: "Buscamos"
        categoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.of(categoria)
        primeraSubCategoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.of(primeraSubCategoria)
        def response = categoriaService.findAllSegundaSubCategoriaByNombreCategoriaAndNombrePrimeraSubCategoria(nombreCategoria, nombrePrimeraSubCategoria)

        then: "Validamos que sea de manera correcta"
        !response.isEmpty()
    }

    def "findAll subcategoria by nombre categoria y primera subcategoria sin categoria existente"() {
        given: "Una solicitud para buscar todas las subcategorias por nombre categoria y primera subcategoria"
        def nombreCategoria = "A"
        def nombrePrimeraSubCategoria = "B"

        when: "Buscamos"
        categoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.empty()
        def response = categoriaService.findAllSegundaSubCategoriaByNombreCategoriaAndNombrePrimeraSubCategoria(nombreCategoria, nombrePrimeraSubCategoria)

        then: "Validamos que venga la lista vacia"
        response.isEmpty()
    }

    def "findAll subcategoria by nombre categoria y primera subcategoria sin primera subcategoria existente"() {
        given: "Una solicitud para buscar todas las subcategorias por nombre categoria y primera subcategoria"
        def nombreCategoria = "A"
        def nombrePrimeraSubCategoria = "B"
        def categoria = Categoria
                .builder()
                .build()

        when: "Buscamos"
        categoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.of(categoria)
        primeraSubCategoriaRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.empty()
        def response = categoriaService.findAllSegundaSubCategoriaByNombreCategoriaAndNombrePrimeraSubCategoria(nombreCategoria, nombrePrimeraSubCategoria)

        then: "Validamos que venga la lista vacia"
        response.isEmpty()
    }

    def "save crear categoria"() {
        given: "Una solicitud de creacion de categoria"
        def crearCategoriaRequestDTO = CategoriaRequestDTO
                .builder()
                .nombre("A")
                .descripcion("B")
                .subCategorias(Collections.singletonList(PrimeraSubCategoriaDTO
                        .builder()
                        .nombre("C")
                        .descripcion("D")
                        .subCategorias(Collections.singletonList(SegundaSubCategoriaDTO
                                .builder()
                                .nombre("E")
                                .descripcion("F")
                                .build()))
                        .build()))
                .build()

        when: "Creamos"
        categoriaRepository.save(_ as Class<?> as Categoria) >> { }
        categoriaRepository.existsCategoriaByNombreContainingIgnoreCase(_ as String) >> true
        primeraSubCategoriaRepository.save(_ as Class<?> as PrimeraSubCategoria) >> { }
        primeraSubCategoriaRepository.existsPrimeraSubCategoriaByNombreContainingIgnoreCase(_ as String) >> true
        segundaSubCategoriaRepository.saveAll(_ as Iterable) >> { }
        def response = categoriaService.save(crearCategoriaRequestDTO)

        then: "Validamos que sea de manera correcta"
        response
    }

    def "save crear categoria sin haber creado la categoria"() {
        given: "Una solicitud de creacion de categoria"
        def crearCategoriaRequestDTO = CategoriaRequestDTO
                .builder()
                .nombre("A")
                .descripcion("B")
                .subCategorias(Collections.singletonList(PrimeraSubCategoriaDTO
                        .builder()
                        .nombre("C")
                        .descripcion("D")
                        .subCategorias(Collections.singletonList(SegundaSubCategoriaDTO
                                .builder()
                                .nombre("E")
                                .descripcion("F")
                                .build()))
                        .build()))
                .build()

        when: "Creamos"
        categoriaRepository.save(_ as Class<?> as Categoria) >> { }
        categoriaRepository.existsCategoriaByNombreContainingIgnoreCase(_ as String) >> false
        def response = categoriaService.save(crearCategoriaRequestDTO)

        then: "Validamos que venga en false"
        !response
    }

    def "save crear categoria sin haber creado la primera subcategoria"() {
        given: "Una solicitud de creacion de categoria"
        def crearCategoriaRequestDTO = CategoriaRequestDTO
                .builder()
                .nombre("A")
                .descripcion("B")
                .subCategorias(Collections.singletonList(PrimeraSubCategoriaDTO
                        .builder()
                        .nombre("C")
                        .descripcion("D")
                        .subCategorias(Collections.singletonList(SegundaSubCategoriaDTO
                                .builder()
                                .nombre("E")
                                .descripcion("F")
                                .build()))
                        .build()))
                .build()

        when: "Creamos"
        categoriaRepository.save(_ as Class<?> as Categoria) >> { }
        categoriaRepository.existsCategoriaByNombreContainingIgnoreCase(_ as String) >> true
        primeraSubCategoriaRepository.save(_ as Class<?> as PrimeraSubCategoria) >> { }
        primeraSubCategoriaRepository.existsPrimeraSubCategoriaByNombreContainingIgnoreCase(_ as String) >> false
        segundaSubCategoriaRepository.saveAll(_ as Iterable) >> { }
        def response = categoriaService.save(crearCategoriaRequestDTO)

        then: "Validamos que sea de manera correcta"
        response
    }
}
