package cl.ecommerce.service

import cl.ecommerce.dto.ImagenDTO
import cl.ecommerce.dto.ProductoRequestDTO
import cl.ecommerce.model.Imagen
import cl.ecommerce.model.Producto
import cl.ecommerce.repository.ImagenRepository
import cl.ecommerce.repository.ProductoRepository
import spock.lang.Specification

class ProductoServiceSpec extends Specification {
    def productoRepository = Stub(ProductoRepository)
    def imagenRepository = Stub(ImagenRepository)
    ProductoService productoService = new ProductoService(productoRepository, imagenRepository)

    def "findAll"() {
        given: "Una solicitud de consulta general"
        def producto = Producto
                .builder()
                .id(1)
                .sku(1)
                .nombre("Ejemplo")
                .descripcion("Ejemplo")
                .precio(100.0)
                .imagenes(Arrays.asList(Imagen
                        .builder()
                        .id(1)
                        .idProducto(1)
                        .url("https://www.img.cl")
                        .descripcion("ejemplo")
                        .build()))
                .build()
        def productos = Arrays.asList(producto)

        when: "Consultamos"
        productoRepository.findAll() >> productos
        def response = productoService.findAll()

        then: "Validamos que sea de manera correcta"
        response != null
        !response.isEmpty()
    }

    def "findById is present"() {
        given: "Una solicitud para encontrar por id"
        def idProducto = 1
        def producto = Producto
                .builder()
                .id(1)
                .sku(1)
                .nombre("Ejemplo")
                .descripcion("Ejemplo")
                .precio(100.0)
                .imagenes(Arrays.asList(Imagen
                        .builder()
                        .id(1)
                        .idProducto(1)
                        .url("https://www.img.cl")
                        .descripcion("ejemplo")
                        .build()))
                .build()

        when: "Consultamos"
        productoRepository.findBySku(_ as Long) >> Optional.of(producto)
        def response = productoService.findBySku(idProducto)

        then: "Validamos que sea de manera correcta"
        response.isPresent()
    }

    def "findById is not present"() {
        given: "Una solicitud para encontrar por id"
        def idProducto = 1

        when: "Consultamos"
        productoRepository.findBySku(_ as Long) >> Optional.empty()
        def response = productoService.findBySku(idProducto)

        then: "Validamos que sea de manera correcta"
        !response.isPresent()
    }

    def "save"() {
        given: "Una solicitud para guardar"
        def productoDTO = ProductoRequestDTO
                .builder()
                .sku(sku)
                .nombre(nombre)
                .descripcion(descripcion)
                .precio(precio)
                .imagenes(Arrays.asList(ImagenDTO
                        .builder()
                        .url("https://www.img.cl")
                        .descripcion("ejemplo")
                        .build()))
                .build()
        def productoCreado = Producto
                .builder()
                .id(id)
                .sku(sku)
                .nombre(nombre)
                .descripcion(descripcion)
                .precio(precio)
                .build()

        when: "Creamos"
        productoRepository.save(_ as Producto) >> productoCreado
        def response = productoService.save(productoDTO)

        then: "Validamos la respuesta"
        response == respuesta

        where:
        id       | sku     | nombre             | descripcion         | precio    | respuesta
        11111111 | 2222222 | "Gameboy Advance"  | "Nintendo Portatil" | 100_000.0 | true
        0        | 2222222 | "Gameboy Advance"  | "Nintendo Portatil" | 100_000.0 | false
    }
}