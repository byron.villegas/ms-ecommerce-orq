package cl.ecommerce.controller

import cl.ecommerce.dto.CategoriaRequestDTO
import cl.ecommerce.dto.CategoriaResponseDTO
import cl.ecommerce.dto.PrimeraSubCategoriaDTO
import cl.ecommerce.service.CategoriaService
import com.google.gson.Gson
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class CategoriaControllerSpec extends Specification {
    def categoriaService = Stub(CategoriaService)
    def mvc = MockMvcBuilders.standaloneSetup(new CategoriaController(categoriaService)).build()
    def gson = new Gson()

    def setup() {

    }

    def "Obtener todas las categorias"() {
        given: "Una solicitud de obtencion de todas las categorias"

        when: "Obtenemos"
        def response = mvc.perform(MockMvcRequestBuilders.get("/categorias")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().response

        then: "Validamos que sea de manera correcta"
        response.status == HttpStatus.OK.value()
    }

    def "Obtener categoria mediante nombre OK"() {
        given: "Una solicitud de obtener categoria mediante nombre"
        def nombreCategoria = "A"

        when: "Obtenemos"
        categoriaService.findByNombre(_ as String) >> Optional.of(CategoriaResponseDTO.builder().build())
        def response = mvc.perform(MockMvcRequestBuilders.get("/categorias/{nombre}", nombreCategoria)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().response

        then: "Validamos que sea de manera correcta"
        response.status == HttpStatus.OK.value()
    }

    def "Obtener categoria mediante nombre con error"() {
        given: "Una solicitud de obtener categoria mediante nombre"
        def nombreCategoria = "A"

        when: "Obtenemos"
        categoriaService.findByNombre(_ as String) >> Optional.empty()
        def response = mvc.perform(MockMvcRequestBuilders.get("/categorias/{nombre}", nombreCategoria)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().response

        then: "Validamos que venga el error"
        response.status == HttpStatus.NOT_FOUND.value()
    }

    def "Obtener segunda sub categoria mediante nombres categoria y primera sub categoria OK"() {
        given: "Una solicitud de mediante nombres categoria y primera sub categoria"
        def nombreCategoria = "A"
        def nombrePrimeraSubCategoria = "B"

        when: "Obtenemos"
        categoriaService.findByNombre(_ as String) >> Optional.of(PrimeraSubCategoriaDTO.builder().build())
        def response = mvc.perform(MockMvcRequestBuilders.get("/categorias/{nombreCategoria}/{nombrePrimeraSubCategoria}", nombreCategoria, nombrePrimeraSubCategoria)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().response

        then: "Validamos que sea de manera correcta"
        response.status == HttpStatus.OK.value()
    }

    def "Guardar categoria OK"() {
        given: "Una solicitud de creacion de categoria"
        def categoriaRequestDTO = CategoriaRequestDTO
                .builder()
                .build()

        when: "Creamos"
        categoriaService.save(_ as CategoriaRequestDTO) >> true
        def response = mvc.perform(MockMvcRequestBuilders.post("/categorias")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(gson.toJson(categoriaRequestDTO)))
                .andReturn().response

        then: "Validamos que sea de manera correcta"
        response.status == HttpStatus.CREATED.value()
    }

    def "Guardar categoria con error"() {
        given: "Una solicitud de creacion de categoria"
        def categoriaRequestDTO = CategoriaRequestDTO
                .builder()
                .build()

        when: "Creamos"
        categoriaService.save(_ as CategoriaRequestDTO) >> false
        def response = mvc.perform(MockMvcRequestBuilders.post("/categorias")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(gson.toJson(categoriaRequestDTO)))
                .andReturn().response

        then: "Validamos que sea de manera correcta"
        response.status == HttpStatus.INTERNAL_SERVER_ERROR.value()
    }
}