package cl.ecommerce.util

import cl.ecommerce.constants.Constants
import spock.lang.Specification

class FileUtilSpec extends Specification {

    def setup() {

    }

    def "Validar existencia de archivo"() {
        given: "Una solicitud de existencia de archivo"

        when: "Obtenemos la existencia"
        def response = FileUtil.isFileExistsInResources(nombre, ruta)

        then: "Validamos"
        response == resultado

        where:
        nombre          | ruta                            | resultado
        "report.jrxml"  | Constants.JASPER_REPORTS_FOLDER | true
        "report1.jrxml" | Constants.JASPER_REPORTS_FOLDER | false
    }
}